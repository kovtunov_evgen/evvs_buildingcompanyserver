﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EVVS_Building_Company_Server.DAL;
using EVVS_Building_Company_Server.Models;
using EVVS_Building_Company_Server.Repository;

namespace EVVS_Building_Company_Server.Controllers
{
    public class LanguageController : Controller
    {
        IRepository<Language> db;

        public LanguageController()
        {
            db = new SQLLanguageRepository();
        }

        public ActionResult Index()
        {
            return View(db.GetList());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Language language)
        {
            if (ModelState.IsValid)
            {
                db.Create(language);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(language);
        }

        public ActionResult Edit(int id)
        {
            Language language = db.Get(id);
            return View(language);
        }
        [HttpPost]
        public ActionResult Edit(Language language)
        {
            if (ModelState.IsValid)
            {
                db.Update(language);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(language);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Language b = db.Get(id);
            return View(b);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        //    private BuildingCompanyContext db = new BuildingCompanyContext();

        //    // GET: Language
        //    public ActionResult Index()
        //    {
        //        return View(db.Languages.ToList());
        //    }

        //    // GET: Language/Details/5
        //    public ActionResult Details(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        Language language = db.Languages.Find(id);
        //        if (language == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(language);
        //    }

        //    // GET: Language/Create
        //    public ActionResult Create()
        //    {
        //        return View();
        //    }

        //    // POST: Language/Create
        //    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Create([Bind(Include = "ID,Description,Locale")] Language language)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Languages.Add(language);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }

        //        return View(language);
        //    }

        //    // GET: Language/Edit/5
        //    public ActionResult Edit(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        Language language = db.Languages.Find(id);
        //        if (language == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(language);
        //    }

        //    // POST: Language/Edit/5
        //    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Edit([Bind(Include = "ID,Description,Locale")] Language language)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Entry(language).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        return View(language);
        //    }

        //    // GET: Language/Delete/5
        //    public ActionResult Delete(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //        Language language = db.Languages.Find(id);
        //        if (language == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(language);
        //    }

        //    // POST: Language/Delete/5
        //    [HttpPost, ActionName("Delete")]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult DeleteConfirmed(int id)
        //    {
        //        Language language = db.Languages.Find(id);
        //        db.Languages.Remove(language);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    protected override void Dispose(bool disposing)
        //    {
        //        if (disposing)
        //        {
        //            db.Dispose();
        //        }
        //        base.Dispose(disposing);
        //    }
        //}
    }
}
