﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EVVS_Building_Company_Server.DAL;
using EVVS_Building_Company_Server.Models;
using EVVS_Building_Company_Server.Repository;
namespace EVVS_Building_Company_Server.Controllers
{
    public class ContentTextController : Controller
    {
        IRepository<ContentText> db;
        public ContentTextController()
        {
            this.db = new SQLContentTextRepository();
        }
        public ActionResult Index()
        {
            return View(db.GetList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ContentText contentText)
        {
            if (ModelState.IsValid)
            {
                db.Create(contentText);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(contentText);
        }

        public ActionResult Edit(int id)
        {
            ContentText contentText = db.Get(id);
            return View(contentText);
        }
        [HttpPost]
        public ActionResult Edit(ContentText contentText)
        {
            if (ModelState.IsValid)
            {
                db.Update(contentText);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(contentText);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ContentText contentText = db.Get(id);
            return View(contentText);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        //private BuildingCompanyContext db = new BuildingCompanyContext();

        //// GET: ContentText
        //public ActionResult Index()
        //{
        //    var contentTexts = db.ContentTexts.Include(c => c.Language).Include(c => c.ContentName);
        //    return View(contentTexts.ToList());
        //}

        //// GET: ContentText/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentText contentText = db.ContentTexts.Find(id);
        //    if (contentText == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contentText);
        //}

        //// GET: ContentText/Create
        //public ActionResult Create()
        //{
        //    ViewBag.LanguageID = new SelectList(db.Languages, "ID", "Description");
        //    ViewBag.ContentID = new SelectList(db.ContentNames, "ID", "Name");

        //    return View();
        //}

        //// POST: ContentText/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,ContentID,LanguageID,Text")] ContentText contentText)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ContentTexts.Add(contentText);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.LanguageID = new SelectList(db.Languages, "ID", "Description", contentText.LanguageID);
        //    ViewBag.ContentID = new SelectList(db.ContentNames, "ID", "Name", contentText.ContentID);

        //    return View(contentText);
        //}

        //// GET: ContentText/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentText contentText = db.ContentTexts.Find(id);
        //    if (contentText == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.LanguageID = new SelectList(db.Languages, "ID", "Description", contentText.LanguageID);
        //    ViewBag.ContentID = new SelectList(db.ContentNames, "ID", "Name", contentText.ContentID);

        //    return View(contentText);
        //}

        //// POST: ContentText/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,ContentID,LanguageID,Text")] ContentText contentText)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(contentText).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.LanguageID = new SelectList(db.Languages, "ID", "Description", contentText.LanguageID);
        //    ViewBag.ContentID = new SelectList(db.ContentNames, "ID", "Name", contentText.ContentID);

        //    return View(contentText);
        //}

        //// GET: ContentText/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentText contentText = db.ContentTexts.Find(id);
        //    if (contentText == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contentText);
        //}

        //// POST: ContentText/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ContentText contentText = db.ContentTexts.Find(id);
        //    db.ContentTexts.Remove(contentText);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
