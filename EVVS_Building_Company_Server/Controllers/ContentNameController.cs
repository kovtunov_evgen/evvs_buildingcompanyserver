﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EVVS_Building_Company_Server.DAL;
using EVVS_Building_Company_Server.Models;
using EVVS_Building_Company_Server.Repository;

namespace EVVS_Building_Company_Server.Controllers
{
    public class ContentNameController : Controller
    {
        IRepository<ContentName> db;
        public ContentNameController()
        {
            this.db = new SQLContentNameRepository();
        }
        public ActionResult Index()
        {
            return View(db.GetList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ContentName contentName)
        {
            if (ModelState.IsValid)
            {
                db.Create(contentName);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(contentName);
        }

        public ActionResult Edit(int id)
        {
            ContentName contentName = db.Get(id);
            return View(contentName);
        }
        [HttpPost]
        public ActionResult Edit(ContentName contentName)
        {
            if (ModelState.IsValid)
            {
                db.Update(contentName);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(contentName);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ContentName contentName = db.Get(id);
            return View(contentName);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        //private BuildingCompanyContext db = new BuildingCompanyContext();

        //// GET: ContentName
        //public ActionResult Index()
        //{
        //    return View(db.ContentNames.ToList());
        //}

        //// GET: ContentName/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentName contentName = db.ContentNames.Find(id);
        //    if (contentName == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contentName);
        //}

        //// GET: ContentName/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: ContentName/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,Name")] ContentName contentName)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ContentNames.Add(contentName);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(contentName);
        //}

        //// GET: ContentName/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentName contentName = db.ContentNames.Find(id);
        //    if (contentName == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contentName);
        //}

        //// POST: ContentName/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,Name")] ContentName contentName)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(contentName).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(contentName);
        //}

        //// GET: ContentName/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ContentName contentName = db.ContentNames.Find(id);
        //    if (contentName == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contentName);
        //}

        //// POST: ContentName/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ContentName contentName = db.ContentNames.Find(id);
        //    db.ContentNames.Remove(contentName);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
