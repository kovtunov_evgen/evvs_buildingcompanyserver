namespace EVVS_Building_Company_Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BuildingCompanyDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentName",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ContentText",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ContentID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                        Text = c.String(),
                        ContentName_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContentName", t => t.ContentName_ID)
                .ForeignKey("dbo.Language", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.LanguageID)
                .Index(t => t.ContentName_ID);
            
            CreateTable(
                "dbo.Language",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Description = c.String(),
                        Locale = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContentText", "LanguageID", "dbo.Language");
            DropForeignKey("dbo.ContentText", "ContentName_ID", "dbo.ContentName");
            DropIndex("dbo.ContentText", new[] { "ContentName_ID" });
            DropIndex("dbo.ContentText", new[] { "LanguageID" });
            DropTable("dbo.Language");
            DropTable("dbo.ContentText");
            DropTable("dbo.ContentName");
        }
    }
}
