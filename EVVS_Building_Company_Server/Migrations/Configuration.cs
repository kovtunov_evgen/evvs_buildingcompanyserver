namespace EVVS_Building_Company_Server.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EVVS_Building_Company_Server.DAL.BuildingCompanyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "EVVS_Building_Company_Server.DAL.BuildingCompanyContext";
        }

        protected override void Seed(EVVS_Building_Company_Server.DAL.BuildingCompanyContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
