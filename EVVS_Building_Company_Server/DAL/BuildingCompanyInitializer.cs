﻿using EVVS_Building_Company_Server.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace EVVS_Building_Company_Server.DAL
{
    public class BuildingCompanyInitializer : DropCreateDatabaseIfModelChanges<BuildingCompanyContext>
    {
        

        protected override void Seed(BuildingCompanyContext context)
        {
            IList<Language> language = new List<Language>();

            language.Add(new Language() {  Description = "UKR", Locale = "Ukrainian" });
            language.Add(new Language() { Description = "RUS", Locale = "Russian" });
            language.Add(new Language() { Description = "ENG", Locale = "English" });

            foreach (Language lan in language)
                context.Languages.Add(lan);

            base.Seed(context);

            IList<ContentName> contentName = new List<ContentName>();

            contentName.Add(new ContentName() {  Name = "Navbar" });
            contentName.Add(new ContentName() {  Name = "Footer" });
            contentName.Add(new ContentName() {  Name = "Container" });

            foreach (ContentName cn in contentName)
                context.ContentNames.Add(cn);

            base.Seed(context);

            IList<ContentText> contentText = new List<ContentText>();

            contentText.Add(new ContentText() {  ContentID = 1, LanguageID = 1, Text = "Hello1" });
            contentText.Add(new ContentText() {  ContentID = 2, LanguageID = 2, Text = "Hello2" });
            contentText.Add(new ContentText() {  ContentID = 3, LanguageID = 3, Text = "Hello3" });

            foreach (ContentText ct in contentText)
                context.ContentTexts.Add(ct);

            base.Seed(context);
        }

        //public override void InitializeDatabase(BuildingCompanyContext context)
        //{
        //    var languages = new List<Language>
        //    {
        //    new Language{ID=1, Description="UKR",Locale="Ukrainian",},
        //    new Language{ID=2,Description="RUS",Locale="Russian",},
        //    new Language{ID=3, Description="ENG",Locale="English",}
        //    };

        //    languages.ForEach(s => context.Languages.Add(s));
        //    context.SaveChanges();
        //    var contents = new List<ContentName>
        //    {
        //    new ContentName{ID=1, Name="Navbar",},
        //    new ContentName{ID=2, Name="Footer",},
        //    new ContentName{ID=3, Name="Container",}
        //    };
        //    contents.ForEach(s => context.ContentNames.Add(s));
        //    context.SaveChanges();
        //    var contenttexts = new List<ContentText>
        //    {
        //    new ContentText{ID=1, ContentID=1,LanguageID=1,Text="Hello1"},
        //    new ContentText{ID=2,ContentID=2,LanguageID=2,Text="Hello2"},
        //   new ContentText{ID=3,ContentID=3,LanguageID=3,Text="Hello3"}
        //    };
        //    contenttexts.ForEach(s => context.ContentTexts.Add(s));
        //    context.SaveChanges();
        //}

       
    }
}