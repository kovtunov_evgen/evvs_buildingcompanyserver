﻿using EVVS_Building_Company_Server.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EVVS_Building_Company_Server.DAL
{
    public class BuildingCompanyContext : DbContext
    {
        public BuildingCompanyContext() : base("BuildingCompanyContext")
        {
            Database.SetInitializer(new BuildingCompanyInitializer());
        }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ContentName> ContentNames { get; set; }
        public DbSet<ContentText> ContentTexts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}