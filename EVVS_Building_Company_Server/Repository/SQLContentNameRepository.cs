﻿using EVVS_Building_Company_Server.DAL;
using EVVS_Building_Company_Server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EVVS_Building_Company_Server.Repository
{
    public class SQLContentNameRepository : IRepository<ContentName>
    {
        private BuildingCompanyContext db;

        public SQLContentNameRepository()
        {
            this.db = new BuildingCompanyContext();
        }
        public IEnumerable<ContentName> GetList()
        {
            return db.ContentNames;
        }
        public ContentName Get(int id)
        {
            return db.ContentNames.Find(id);
        }
        public void Create(ContentName contentName)
        {
           db.ContentNames.Add(contentName);
        }
        public void Update(ContentName contentName)
        {
            db.Entry(contentName).State = EntityState.Modified;
        }
        public void Delete(int id)
        {
            ContentName contentName = db.ContentNames.Find(id);
            if (contentName != null)
                db.ContentNames.Remove(contentName);
        }
        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }








    }
}