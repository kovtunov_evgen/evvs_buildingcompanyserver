﻿using EVVS_Building_Company_Server.DAL;
using EVVS_Building_Company_Server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EVVS_Building_Company_Server.Repository
{
    public class SQLContentTextRepository : IRepository<ContentText>
    {
        private BuildingCompanyContext db;
        public SQLContentTextRepository() {
            this.db =new BuildingCompanyContext();
        }

        public IEnumerable<ContentText> GetList()
        {
            return db.ContentTexts;
        }
        public ContentText Get(int id)
        {
           return db.ContentTexts.Find(id);
        }
        public void Create(ContentText contentText)
        {
           db.ContentTexts.Add(contentText);
        }

        public void Delete(int id)
        {
            ContentText contentText = db.ContentTexts.Find(id);
            if (contentText != null)
                db.ContentTexts.Remove(contentText);
        }
        public void Update(ContentText contentText)
        {
            db.Entry(contentText).State = EntityState.Modified;
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }





        public void Save()
        {
           db.SaveChanges();
        }

       
    }
}