﻿using EVVS_Building_Company_Server.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using EVVS_Building_Company_Server.Models;

namespace EVVS_Building_Company_Server.Repository
{
    
    public class SQLLanguageRepository:IRepository<Language>
    {
        private BuildingCompanyContext db;

        public SQLLanguageRepository()
        {
            this.db = new BuildingCompanyContext();
        }

        public IEnumerable<Language> GetList()
        {
            return db.Languages;
        }
        public Language Get(int id)
        {
            return db.Languages.Find(id);
        }
        public void Create(Language language)
        {
            db.Languages.Add(language);
        }
        public void Update(Language language)
        {
            db.Entry(language).State = EntityState.Modified;
        }
        public void Delete(int id)
        {
            Language language = db.Languages.Find(id);
            if (language != null)
                db.Languages.Remove(language);
        }
        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      

        

        
    }
}