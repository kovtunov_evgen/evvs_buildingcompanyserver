﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
namespace EVVS_Building_Company_Server.Models
{
    public class ContentName
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ContentText> ContentTexts { get; set; }
    }
}