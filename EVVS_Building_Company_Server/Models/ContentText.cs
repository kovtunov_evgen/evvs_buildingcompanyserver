﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVVS_Building_Company_Server.Models
{
    public class ContentText
    {
        public int ID { get; set; }
        public int ContentID { get; set; }
        public int LanguageID { get; set; }
        public string Text { get; set; }

        public virtual ContentName ContentName { get; set; }
        public virtual Language Language { get; set; }
    }
}