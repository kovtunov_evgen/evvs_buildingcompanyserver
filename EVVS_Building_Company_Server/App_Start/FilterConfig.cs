﻿using System.Web;
using System.Web.Mvc;

namespace EVVS_Building_Company_Server
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
